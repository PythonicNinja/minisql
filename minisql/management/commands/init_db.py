import random

from django.core.management.base import BaseCommand
from users.models import Student, Teacher, User
from course.models import Course
from grade.models import Grade

class Command(BaseCommand):
    help = "Fill in db"

    def create_teacher(self, n=10):
        teachers = []
        for i in range(n):
            teacher = Teacher.objects.create(
                department="teaching",
                user=User.objects.create_user(**{
                    'username': "teacher{}".format(i),
                    'password': "{}".format(i)
                })
            )
            teachers.append(teacher)
        return teachers

    def create_students(self, n=10):
        students = []
        for i in range(n):
            student = Student.objects.create(
                user=User.objects.create_user(**{
                    'username': "student{}".format(i),
                    'password': "{}".format(i)
                })
            )
            students.append(student)
        return students

    def create_courses(self, teacher, students, n=10):
        courses = []
        for i in range(n):
            course = Course.objects.create(
                topic="topic{}".format(i),
                teacher=teacher,
            )
            courses.append(course)
        return courses

    def create_grades(self, student, teacher, course, n=10):
        grades = []
        for i in range(n):
            grade = Grade.objects.create(
                student=student,
                teacher=teacher,
                course=course,
                grade=random.choice([x for x, y in Grade._meta.get_field('grade').get_choices() if x])
            )
            grades.append(grade)

        return grades

    def handle(self, *args, **options):
        teachers = self.create_teacher(10)
        students = self.create_students(10)
        courses = self.create_courses(teachers[0], students, 20)
        grades = self.create_grades(students[0], teachers[0], courses[0], n=100)


