from django_tables2 import tables

from .models import Course


class CourseTable(tables.Table):
    actions = tables.columns.TemplateColumn('''
    {% if request.user.teacher %}
    <a href={% url "course:update" record.pk %}>Update</a>
    <a href={% url "course:delete" record.pk %}>Delete</a>
    {% endif %}
    ''', orderable=False)

    class Meta:
        model = Course
