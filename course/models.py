from __future__ import unicode_literals

from django.db import models


class Course(models.Model):
    topic = models.CharField(max_length=255)
    teacher = models.ForeignKey('users.Teacher')
    students = models.ManyToManyField('users.Student')

    def __unicode__(self):
        return u"{} - {}".format(self.topic, self.teacher)
