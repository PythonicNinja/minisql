from django.conf.urls import url

from .views import CourseListView, CourseCreateView, CourseDeleteView, CourseUpdateView

app_name = 'course'
urlpatterns = [
    url(r'^$', CourseListView.as_view(), name='list'),
    url(r'^create$', CourseCreateView.as_view(), name='create'),
    url(r'^(?P<pk>\d+)/update$', CourseUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete$', CourseDeleteView.as_view(), name='delete'),
]
