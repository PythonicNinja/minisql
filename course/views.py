from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView
from django_tables2 import SingleTableView
from grade.views import TeacherViewMixin

from .models import Course
from .tables import CourseTable


class CourseListView(LoginRequiredMixin, SingleTableView):
    model = Course
    table_class = CourseTable

    def get_queryset(self):
        qs = super(CourseListView, self).get_queryset()
        user = self.request.user
        if hasattr(user, 'student'):
            qs = qs.filter(students=user.student)
        if hasattr(user, 'teacher'):
            qs = qs.filter(teacher=user.teacher)
        return qs


class CourseCreateView(TeacherViewMixin, CreateView):
    model = Course
    fields = ('topic', 'students')
    success_url = reverse_lazy('course:list')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.teacher = self.request.user.teacher
        return super(CourseCreateView, self).form_valid(form)


class CourseUpdateView(TeacherViewMixin, UpdateView):
    model = Course
    fields = ('topic', 'students')
    success_url = reverse_lazy('course:list')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.teacher = self.request.user.teacher
        return super(CourseUpdateView, self).form_valid(form)


class CourseDeleteView(TeacherViewMixin, DeleteView):
    model = Course
    success_url = reverse_lazy('course:list')
