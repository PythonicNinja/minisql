from django.conf.urls import url

from .views import GradeListView, GradeCreateView, GradeUpdateView, GradeDeleteView

app_name = 'grades'
urlpatterns = [
    url(r'^$', GradeListView.as_view(), name='list'),
    url(r'^create$', GradeCreateView.as_view(), name='create'),
    url(r'^(?P<pk>\d+)/update$', GradeUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete$', GradeDeleteView.as_view(), name='delete'),
]
