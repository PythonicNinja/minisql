from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView
from django_tables2 import SingleTableView

from .models import Grade
from .tables import GradeTable


class GradeListView(LoginRequiredMixin, SingleTableView):
    model = Grade
    table_class = GradeTable

    def get_queryset(self):
        qs = super(GradeListView, self).get_queryset()
        user = self.request.user
        if hasattr(user, 'student'):
            qs = qs.filter(student=user.student)
        if hasattr(user, 'teacher'):
            qs = qs.filter(teacher=user.teacher)
        return qs


class TeacherViewMixin(UserPassesTestMixin, LoginRequiredMixin):
    def test_func(self):
        return hasattr(self.request.user, 'teacher')


class GradeCreateView(TeacherViewMixin, CreateView):
    model = Grade
    fields = ('student', 'grade', 'course')
    success_url = reverse_lazy('grades:list')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.teacher = self.request.user.teacher
        return super(GradeCreateView, self).form_valid(form)


class GradeUpdateView(TeacherViewMixin, UpdateView):
    model = Grade
    fields = ('student', 'grade', 'course')
    success_url = reverse_lazy('grades:list')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.teacher = self.request.user.teacher
        return super(GradeUpdateView, self).form_valid(form)


class GradeDeleteView(TeacherViewMixin, DeleteView):
    model = Grade
    success_url = reverse_lazy('grades:list')

