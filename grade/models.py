from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Grade(models.Model):
    grade_choices = (
        [str(i) + sign, str(i) + sign]
        for i in range(1, 7)
        for sign in ['', '+', '-']
        if not (i == 1 and sign == '-') and not (i == 6 and sign == '+')
    )
    student = models.ForeignKey('users.Student', null=True)
    teacher = models.ForeignKey('users.Teacher', null=True)
    course = models.ForeignKey('course.Course')
    grade = models.CharField(verbose_name=_('Grade'), choices=grade_choices, max_length=3)
