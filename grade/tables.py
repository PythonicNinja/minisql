from django_tables2 import tables

from .models import Grade


class GradeTable(tables.Table):
    actions = tables.columns.TemplateColumn('''
    {% if request.user.teacher %}
    <a href={% url "grades:update" record.pk %}>Update</a>
    <a href={% url "grades:delete" record.pk %}>Delete</a>
    {% endif %}
    ''', orderable=False)

    class Meta:
        model = Grade
