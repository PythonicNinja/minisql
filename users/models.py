from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    department = models.CharField(verbose_name=_('Departament'), max_length=100, blank=True, null=True)

    def __unicode__(self):
        return u"{} - {} {} @ {}".format(self.user.username, self.user.first_name, self.user.last_name, self.department)


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __unicode__(self):
        return u"{} - {} {}".format(self.user.username, self.user.first_name, self.user.last_name)
