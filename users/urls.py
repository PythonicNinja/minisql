from django.conf.urls import url
from django.contrib.auth.views import login, logout

app_name = 'users'
urlpatterns = [
    url(r'^login$', login, {'template_name': 'users/login.html'}, name='login'),
    url(r'^logout$', logout, {'template_name': 'users/logout.html'}, name='logout'),
]
